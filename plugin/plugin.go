package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks if the filename is Gemfile.lock
func Match(path string, info os.FileInfo) (bool, error) {
	if info.Name() == "Gemfile.lock" {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("bundler-audit", Match)
}
