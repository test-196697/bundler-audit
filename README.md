# bundler-audit analyzer

Dependency Scanning for Ruby projects. It's based on [bundler-audit](https://github.com/rubysec/bundler-audit).

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## How to update the upstream Scanner

- Check for the latest version at https://github.com/rubysec/bundler-audit/tags
- Compare with the value of `SCANNER_VERSION` in the [Dockerfile](./Dockerfile)
- If an update is available, create a branch and bump the version.
- Trigger a pipeline on all relevant [test projects](https://gitlab.com/explore/projects?tag=Dependency+Scanning,Secure-QA):
    - trigger a manual pipeline on `master` branch with these variables:
      - `DS_DEFAULT_ANALYZERS`: "" (just leave the form input blank to send an empty string)
      - `DS_ANALYZER_IMAGES`: "_docker_image_tag_for_your_branch_,registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium:2" ([See documentation](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/analyzers.html#custom-analyzers))
          
          There's one gotcha here: on top of your custom analyzer image, you also need to give the usual Genmnasium analyzer image otherwise the QA may fail.

## Running the analyzer in airgapped or internal networks

By default, this analyzer will make a network call to update its internal advisory DB (https://github.com/rubysec/ruby-advisory-db.git) at scan time.

1. To prevent any scan-time updates use `BUNDLER_AUDIT_UPDATE_DISABLED="true"`.
2. To ensure the advisory DB is up to date against a fork you control, use `BUNDLER_AUDIT_ADVISORY_DB_URL` and `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME`.

Note:
* `BUNDLER_AUDIT_ADVISORY_DB_URL` is the Git URL of the advisory DB `bundler-audit` gem uses (forked from https://github.com/rubysec/ruby-advisory-db.git repository and following the same internal structure).
* `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME` is Git the ref name in the advisory DB above (can be a commit hash, a branch, or a tag name).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
