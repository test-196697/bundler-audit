package main

import (
	"bufio"
	"io"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	filename = "Gemfile.lock"
)

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	vulns := parse(reader)
	issues := make([]issue.Issue, len(vulns))
	path := filepath.Join(prependPath, filename)
	for i, vuln := range vulns {
		issues[i] = vuln.Issue(path)
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}

func parse(reader io.Reader) []Vulnerability {

	vulns := []Vulnerability{} // found vulnerabilities
	var v *Vulnerability       // vulnerability currently parsed

	// to allocate a vulnerability if needed
	var allocate = func() *Vulnerability {
		if v == nil {
			v = &Vulnerability{}
		}
		return v
	}

	// to append current vulnerability if any
	var flush = func() {
		if v != nil {
			vulns = append(vulns, *v)
			v = nil // reset current vulnerability
		}
	}

	// scan document
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()

		// append vuln if empty line
		if line == "" {
			flush()
			continue
		}

		// parse fields
		if fields := strings.SplitN(line, ":", 2); len(fields) > 1 {
			key := strings.TrimSpace(fields[0])
			value := strings.TrimSpace(fields[1])
			vuln := allocate()
			switch key {
			case "Name":
				vuln.Name = value
			case "Version":
				vuln.Version = value
			case "Advisory":
				vuln.Advisory = value
			case "Criticality":
				vuln.Criticality = value
			case "URL":
				vuln.URL = value
			case "Title":
				vuln.Title = value
			case "Solution":
				vuln.Solution = value
			}
		}
	}

	// flush in case there's no empty line after last vulnerability
	flush()

	return vulns
}

// Vulnerability describes a vulnerability found by bundler-audit.
type Vulnerability struct {
	Name        string
	Version     string
	Advisory    string
	Criticality string
	URL         string
	Title       string
	Solution    string
}

// Issue converts a vulnerability into a generic issue.
func (v Vulnerability) Issue(filepath string) issue.Issue {
	return issue.DependencyScanningVulnerability{issue.Issue{
		Message:  v.Title,
		Severity: issue.ParseSeverityLevel(v.Criticality),
		Category: issue.CategoryDependencyScanning,
		Scanner:  metadata.IssueScanner,
		Location: issue.Location{
			File: filepath,
			Dependency: &issue.Dependency{
				Package: issue.Package{
					Name: v.Name,
				},
				Version: v.Version,
			},
		},
		Identifiers: v.identifiers(),
		Links:       issue.NewLinks(v.URL),
		Solution:    v.Solution,
	}}.ToIssue()
}

func (v Vulnerability) identifiers() []issue.Identifier {
	ids := []issue.Identifier{}
	if id, ok := issue.ParseIdentifierID(v.AdvisoryID()); ok {
		ids = append(ids, id)
	}
	return ids
}

// AdvisoryID sanitizes the advisory field and prepend "OSVDB-" when needed.
func (v Vulnerability) AdvisoryID() string {
	a := v.Advisory
	if len(a) > 0 && !strings.Contains(a, "-") {
		return "OSVDB-" + a
	}
	return a
}
