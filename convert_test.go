package main

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestConvert(t *testing.T) {
	in := `

Advisory: CVE-2018-1000201
Version: 1.9.18
Name: ffi
Criticality: High
URL: https://github.com/ffi/ffi/releases/tag/1.9.24
Title: ruby-ffi DDL loading issue on Windows OS
Solution: upgrade to >= 1.9.24

Name: loofah
Version: 2.1.1
Advisory: CVE-2018-16468
Criticality: Unknown
URL: https://github.com/flavorjones/loofah/issues/154
Title: Loofah XSS Vulnerability
Solution: upgrade to >= 2.2.3


Name: mail
Version: 2.4.4
Advisory: 131677
Criticality: Unknown
URL: https://hackerone.com/reports/137631
Title: SMTP command injection
Solution: upgrade to >= 2.5.5

Vulnerabilities found! `

	var scanner = metadata.IssueScanner

	r := strings.NewReader(in)
	want := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{
				Category:   issue.CategoryDependencyScanning,
				Message:    "ruby-ffi DDL loading issue on Windows OS",
				CompareKey: "app/Gemfile.lock:ffi:cve:CVE-2018-1000201",
				Severity:   issue.SeverityLevelHigh,
				Scanner:    scanner,
				Location: issue.Location{
					File: "app/Gemfile.lock",
					Dependency: &issue.Dependency{
						Package: issue.Package{Name: "ffi"},
						Version: "1.9.18",
					},
				},
				Identifiers: []issue.Identifier{
					{
						Type:  issue.IdentifierTypeCVE,
						Name:  "CVE-2018-1000201",
						Value: "CVE-2018-1000201",
						URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1000201",
					},
				},
				Links: []issue.Link{
					{
						URL: "https://github.com/ffi/ffi/releases/tag/1.9.24",
					},
				},
				Solution: "upgrade to >= 1.9.24",
			},
			{
				Category:   issue.CategoryDependencyScanning,
				Message:    "Loofah XSS Vulnerability",
				CompareKey: "app/Gemfile.lock:loofah:cve:CVE-2018-16468",
				Severity:   issue.SeverityLevelUnknown,
				Scanner:    scanner,
				Location: issue.Location{
					File: "app/Gemfile.lock",
					Dependency: &issue.Dependency{
						Package: issue.Package{Name: "loofah"},
						Version: "2.1.1",
					},
				},
				Identifiers: []issue.Identifier{
					{
						Type:  issue.IdentifierTypeCVE,
						Name:  "CVE-2018-16468",
						Value: "CVE-2018-16468",
						URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16468",
					},
				},
				Links: []issue.Link{
					{
						URL: "https://github.com/flavorjones/loofah/issues/154",
					},
				},
				Solution: "upgrade to >= 2.2.3",
			},
			{
				Category:   issue.CategoryDependencyScanning,
				Message:    "SMTP command injection",
				CompareKey: "app/Gemfile.lock:mail:osvdb:OSVDB-131677",
				Severity:   issue.SeverityLevelUnknown,
				Scanner:    scanner,
				Location: issue.Location{
					File: "app/Gemfile.lock",
					Dependency: &issue.Dependency{
						Package: issue.Package{Name: "mail"},
						Version: "2.4.4",
					},
				},
				Identifiers: []issue.Identifier{
					{
						Type:  issue.IdentifierTypeOSVDB,
						Name:  "OSVDB-131677",
						Value: "OSVDB-131677",
						URL:   "https://cve.mitre.org/data/refs/refmap/source-OSVDB.html",
					},
				},
				Links: []issue.Link{
					{
						URL: "https://hackerone.com/reports/137631",
					},
				},
				Solution: "upgrade to >= 2.5.5",
			},
		},
		DependencyFiles: []issue.DependencyFile{},
		Remediations:    []issue.Remediation{},
	}
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
