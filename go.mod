module gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli v1.22.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.15.0
)

go 1.13
