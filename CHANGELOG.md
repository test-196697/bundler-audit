# bundler-audit analyzer changelog

## v2.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!50)

## v2.8.3
- Update common to `v2.14.0` which allows git to use CA Certificate bundle (!49)

## v2.8.2
- Use scanner instead of analyzer in `scan.scanner` object (!48)

## v2.8.1
- Upgrade bundler audit to  0.7.0.1 (!46)
- Upgrade bundler to  2.0.0 (!46)

## v2.8.0
- Add scan object to report (!45)

## v2.7.1
- Fix missing CLI arguments in log, when log level is `debug` (!43)

## v2.7.0
- Use [logrus](https://github.com/sirupsen/logrus) combined with [common/logutil](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/tree/v2.10.1/logutil) to improve log messages (!42)
- Filter out log messages based on `SECURE_LOG_LEVEL` (!42)

## v2.6.0
- Upgrade from ruby 2.5 to ruby 2.7, and use `ruby:2.7-slim` (Debian Buster Slim) as a base Docker image (!41)

## v2.5.0
- Add `id` field to vulnerabilities in JSON report (!36)

## v2.4.1
- Fix git error when setting `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME` to a commit hash (!32).

## v2.4.0
- Add support for custom CA certs (!31)

## v2.3.0
- Add `BUNDLER_AUDIT_ADVISORY_DB_URL` and `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME` variables, used to customize the advisory db to be used by the `bundler-audit` gem.

## v2.2.0
- Add `BUNDLER_AUDIT_UPDATE_DISABLED` variable, set this variable true so that bundler-audit does not auto update.

## v2.1.2
- Upgrade common to v.2.1.6

## v2.1.1
- Upgrade common to v.2.1.4, add remediations key and stable sort

## v2.1.0
- Upgrade to bundler-audit 0.6.1, bundler 2.0.1 (Jorn van de beek)

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve compare key, remove vulnerability name

## v1.0.0
- Initial release
